"uses strict";
import { Item } from "./item";

export class Equipment {
  constructor(params) {
    this.weapon = params.weapon;
    this.armor = params.armor;
    this.inventory = params.inventory;
  }

  useRope() {
    for (let index = this.inventory.length - 1; index >= 0; index--) {
      if (this.inventory[index].name === 'rope') {
        this.inventory.splice(index, 1);
      }
    }
  }
  useBossPo() {
    for (let index = this.inventory.length - 1; index >= 0; index--) {
      let invItem = this.inventory[index].name;
      if (invItem === 'BossPo') {
        this.inventory.splice(index, 1);
        agility ++;
        strength ++;
        intelilgence --;
        hp += 12;
      }
    }
  }
  useHealPo() {
    for (let index = this.inventory.length - 1; index >= 0; index--) {
      let invItem = this.inventory[index].name;
      if (invItem === 'healPo') {
        this.inventory.splice(index, 1);
      }
    }
  }
  useKey() {
    for (let index = this.inventory.length - 1; index >= 0; index--) {
      let invItem = this.inventory[index].name;
      if (invItem === 'key') {
        this.inventory.splice(index, 1);
      }
    }
  }
  useTorch() {
    for (let index = this.inventory.length - 1; index >= 0; index--) {
      let invItem = this.inventory[index].name;
      if (invItem === 'torch') {
        this.inventory.splice(index, 1);
      }
    }
  }
  useMedal () {
    for (let index = this.inventory.length - 1; index >= 0; index--) {
      let invItem = this.inventory[index].name;
      if (invItem === 'medal') {
        this.inventory.splice(index, 1);
      }
    }
  }
  useCompass () {
    for (let index = this.inventory.length - 1; index >= 0; index--) {
      let invItem = this.inventory[index].name;
      if (invItem === 'compass') {
        this.inventory.splice(index, 1);
      }
    }
  }

  /**@param {Item} newItem */
  storeItem (newItem) {
    if (newItem instanceof Item) {
      this.inventory.push(newItem);
  }
}
}
