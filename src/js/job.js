export class Job {
  constructor(params) {
    this.description = params.description;
    this.agility = params.agility;
    this.intelligence = params.intelligence;
    this.strength = params.strength;
    this.diceValue = params.diceValue;
  }

  attack() {

    if (this.job === 'warrior') {
      this.throwDice(this.strength);
    }

    else if (this.job === 'wizard') {
      this.throwDice(this.intelligence);
    }

    else {
      this.throwDice(this.agility);
    }
  }
  throwDice(stat) {
    this.diceValue = [];
    for (let index = 0; index < stat; index++) {
      let dice = (1 + Math.floor(Math.random() * 6));
      this.diceValue.push(dice);
      dice = "";
    }
    for (let i = 0; i < this.diceValue.length; i++) {
      let storeValues = this.diceValue[i];
    }
  }

  defense() {
    this.throwDice(this.agility);
  }
  intActions() {
    this.throwDice(this.intelligence);
  }
  agiActions() {
    this.throwDice(this.agility);
  }
  phyActions() {
    this.throwDice(this.strength);
  }
}