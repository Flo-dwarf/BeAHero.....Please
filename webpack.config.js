module.exports = {
  mode: 'development',
  entry: {
    index : './src/js/main',
    game: './src/js/game'
  },
  output: {
    filename: './[name].bundle.js'
  },
  devtool: 'inline-source-map'
}